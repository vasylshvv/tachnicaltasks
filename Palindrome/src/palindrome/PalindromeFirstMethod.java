package palindrome;

import java.util.Scanner;

	
public class PalindromeFirstMethod {
	
	public void checkPalndrome(String inputStr){
		String outputStr = "";
		for (int i = inputStr.length()-1; i >= 0; i--) {
			outputStr = outputStr + inputStr.charAt(i); 
		}
		if(inputStr.equalsIgnoreCase(outputStr)){
			System.out.println("You enter text is Palindrome");
		} else {
			System.out.println("You enter text is not Palindrome");
		}
	}
	
	public static void main(String[] args) {
		String inputStrLine = "";
		Scanner scanner = new Scanner(System.in);
		System.out.println("Please enter string to check for palindrome");
		inputStrLine = scanner.nextLine();
		
		PalindromeFirstMethod method = new PalindromeFirstMethod();
		method.checkPalndrome(inputStrLine);
	}
}
