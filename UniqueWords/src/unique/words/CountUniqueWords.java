package unique.words;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class CountUniqueWords {
public static void main(String[] unused) {

		
		int uniqWordsCount = 0;
		try {

			File inputfile = new File("a.txt");
			Scanner allWords = new Scanner(inputfile);
			allWords.useDelimiter("[^A-Za-z]+");

			HashMap<String, Integer> wordsUniq = new HashMap<String, Integer>();
			while (allWords.hasNext()) {
				String allStr = allWords.next().toLowerCase().toString();
				
				if (wordsUniq.containsKey(allStr)) {
					int value = wordsUniq.get(allStr);
					wordsUniq.put(allStr, value+1);
				} else {
					wordsUniq.put(allStr, 1);					
				}
			}
			Object[] key = wordsUniq.keySet().toArray();
			
			Arrays.sort(key);
			for (int i = 0; i < key.length; i++) {
				System.out.println(key[i] + "\t" + wordsUniq.get(key[i]) + " ");
			}
			
			String strN = "";
			Scanner scanner = new Scanner(System.in);
			System.out.println("Please enter  N top uniqie N=");
		
			strN = scanner.nextLine();
			int N = Integer.parseInt(strN);
			if(N > key.length){
				System.out.println(" N must be <="+ key.length);							
			} else {
				for (int i = 0; i < N; i++) {
					System.out.println(key[i] + "\t" + wordsUniq.get(key[i]) + " ");
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
